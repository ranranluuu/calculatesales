package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	//商品別定義ファイル
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました。";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません。";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です。";
	private static final String FILES_NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません。";
	private static final String FILE_NOT_FORMAT = "のフォーマットが不正です。";
	private static final String BRANCH_CODE_NOT_FORMAT = "の支店コードが不正です。";
	private static final String COMMODITY_CODE_NOT_FORMAT = "の商品コードが不正です。";
	private static final String SALEAMOUNT_OVER = "合計金額が10桁を超えました。";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		//商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		//商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();


		String branchRegex = "^[0-9]{3}$";

		String commodityRegex = "^[0-9a-zA-Z]{8}$";

		//コマンドライン引数が渡されているか
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, branchRegex, "支店")) {
			return;
		}

		//商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, commodityRegex, "商品")) {
			return;
		}


		//listFilesでpathにあるファイルをすべてfilesに格納
		File[] files = new File(args[0]).listFiles();

		//正規表現にmatchしたものだけArrayListに格納
		ArrayList<File> rcdFiles = new ArrayList<>();
		for (int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();
			if (fileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//ソート
		Collections.sort(rcdFiles);

		//格納されたrcdFilesが連番になっているか
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));
			if ((latter - former) != 1) {
				System.out.println(FILES_NOT_SERIAL_NUMBER);
				return;
			}
		}

		//rcdFilesに格納されたファイルから支店別に合計金額を算出
		for (int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				String line;
				ArrayList<String> lines = new ArrayList<String>();
				while((line = br.readLine()) != null) {
					lines.add(line);
				}

				String fileName = rcdFiles.get(i).getName();

				//売り上げ集計ファイルが3行か
				if (lines.size() != 3) {
					System.out.println(fileName + FILE_NOT_FORMAT);
					return;
				}

				//mapにkeyがあるかどうか
				if (!branchNames.containsKey(lines.get(0))) {
					System.out.println(fileName + BRANCH_CODE_NOT_FORMAT);
					return;
				}

				if (!commodityNames.containsKey(lines.get(1))) {
					System.out.println(fileName +  COMMODITY_CODE_NOT_FORMAT);
					return;
				}

				//売上金額の部分が数字なのか
				if (!lines.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//Longにキャストし、合計金額を算出
				long fileSale = Long.parseLong(lines.get(2));
				Long branchSaleAmount = branchSales.get(lines.get(0)) + fileSale;
				Long commoditySaleAmount = commoditySales.get(lines.get(1)) + fileSale;

				//合計金額が10桁を超えていないか
				if ((branchSaleAmount >= 10000000000L) || (commoditySaleAmount >= 10000000000L)) {
					System.out.println(SALEAMOUNT_OVER);
					return;
				}

				//合計金額を更新
				branchSales.put(lines.get(0), branchSaleAmount);
				commoditySales.put(lines.get(1), commoditySaleAmount);


			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;

			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}


	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> fileNames,
										Map<String, Long> fileSales, String Regex, String difinitionFile) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			//ファイルが存在しなければコンソールにエラー文を表示させ、falseを返す
			if (!file.exists()) {
				System.out.println(difinitionFile + FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");

				//読み込んだ行の結果は２行か、また正規表現にコードがマッチしているか
				if ((items.length != 2) || (!items[0].matches(Regex))) {
					System.out.println(difinitionFile + FILE_INVALID_FORMAT);
					return false;
				}


				fileNames.put(items[0], items[1]);
				fileSales.put(items[0], 0L);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> fileNames, Map<String, Long> fileSales) {
		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//ファイルに書き込み処理(keyの数だけ)
			for (String key : fileNames.keySet()) {
				bw.write(key + "," + fileNames.get(key) + "," + fileSales.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
